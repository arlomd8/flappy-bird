﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generate : MonoBehaviour
{
    public GameObject obstacle, fruits;
    public int score = 0;
    public void Spawn()
    {
        InvokeRepeating("CreateObstacle", 1f, 1.5f);
        InvokeRepeating("CreateFruit", 10f, 10f);
    }

    void CreateFruit()
    {
        Instantiate(fruits);
    }

    void OnGUI()
    {
        if (GameManager.instance.gameStart)
        {
            GUI.color = Color.black;
            GUILayout.Label("HARI KE : " + score.ToString());
        }
    }
    void CreateObstacle()
    {
        if (GameManager.instance.gameStart)
        {
            Instantiate(obstacle);
            score++;
            GameManager.instance.score.text = "ARLO BERHASIL DIET SELAMA " + score + " HARI";
        }
        else
            GameManager.instance.score.text = "ARLO BERHASIL DIET SELAMA " + score + " HARI";
    }
}
