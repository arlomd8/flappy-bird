﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool gameStart;
    public static GameManager instance;
    public GameObject menu, gameOver;
    public UnityEngine.UI.Text score;
    void Start()
    {
        gameStart = false;
        gameOver.SetActive(false);
    }
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
    public void ButtonClicked()
    {
        gameStart = true;
        menu.SetActive(false);
    }
    public void ButtonClicked2()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
