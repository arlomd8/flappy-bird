﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Vector2 jumpForce = new Vector2(0, 100);
    public Generate generate;
    
    void Update()
    {
        if (GameManager.instance.gameStart)
        {
            GetComponent<Rigidbody2D>().isKinematic = false;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                GetComponent<Rigidbody2D>().AddForce(jumpForce);
            }

            Vector2 screenPostion = Camera.main.WorldToScreenPoint(transform.position);
            if (screenPostion.y > Screen.height || screenPostion.y < 0)
            {
                Die();
            }
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0,0));
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Fruit"))
        {
            generate.score *= 2;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Die();
    }
    void Die()
    {
        GameManager.instance.gameStart = false;
        GameManager.instance.gameOver.SetActive(true);
    }
}
